import logging
import re
import threading

regex_for_competitor_competition_data = '^(\d+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}),(.+),\d+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12},.+,.+'
regex_for_mapping_data = '^(\d+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}),.+'

logging.basicConfig(format='%(levelname)s:%(asctime)s:%(message)s', level=logging.INFO,
                    datefmt='%m/%d/%Y %I:%M:%S:%ms %p')


def load_data(data, file_url):
    logging.info('import of %s started.', file_url)
    for line in open(file_url, 'r'):
        if not line.__contains__('Test_'):
            data.append(str(line))
    logging.info('import of %s ended.', file_url)


threads = []
threadLock = threading.Lock()
# change yo prefering regex for filtering
pattern = '.+'


class extractDataThread(threading.Thread):
    def __init__(self, thread_name, super_set_item, sub_set, result_data, pattern):
        threading.Thread.__init__(self)
        self.super_set_item = super_set_item
        self.sub_set = sub_set
        self.result_data = result_data
        self.thread_name = thread_name
        self.pattern = pattern

    def run(self):
        if not re.match(regex_for_competitor_competition_data, self.super_set_item):
            return
        to_add = True
        if re.match(self.pattern, str(self.super_set_item)):
            for sub_set_item in self.sub_set:
                if not re.match(regex_for_competitor_competition_data, sub_set_item):
                    continue
                wh_id_super_set = re.search(regex_for_competitor_competition_data, self.super_set_item).group(1)
                name_super_set = re.search(regex_for_competitor_competition_data, self.super_set_item).group(2)
                wh_id_sub_set = re.search(regex_for_competitor_competition_data, sub_set_item).group(1)
                name_sub_set = re.search(regex_for_competitor_competition_data, sub_set_item).group(2)
                if wh_id_super_set == wh_id_sub_set:
                    to_add = False
                    break
                elif name_super_set == name_sub_set:
                    to_add = False
                    break
        if to_add:
            threadLock.acquire()
            self.result_data.append(str(self.super_set_item))
            threadLock.release()


class extractMappingDataThread(threading.Thread):
    def __init__(self, thread_name, mapping_set_item, data_set, result_data):
        threading.Thread.__init__(self)
        self.mapping_set_item = mapping_set_item
        self.data_set = data_set
        self.result_data = result_data
        self.thread_name = thread_name

    def run(self):
        if not re.match(regex_for_mapping_data, self.mapping_set_item):
            return
        to_add = False
        for data_set_item in self.data_set:
            if not re.match(regex_for_competitor_competition_data, data_set_item):
                continue
            mapping_set_item_id = re.search(regex_for_mapping_data, self.mapping_set_item).group(1)
            data_set_item_id = re.search(regex_for_competitor_competition_data, data_set_item).group(1)
            if mapping_set_item_id == data_set_item_id:
                to_add = True
                break
        if to_add:
            threadLock.acquire()
            self.result_data.append(str(self.mapping_set_item))
            threadLock.release()


def extract_data(super_set, sub_set, result_data, extraction_data_name):
    logging.info('extraction of data for %s started.', extraction_data_name)
    thread_name = 0
    for super_set_item in super_set:
        thread_name = thread_name + 1
        thread = extractDataThread(thread_name, super_set_item, sub_set, result_data, pattern)
        logging.debug('thread %s started', thread.name)
        thread.start()
        threads.append(thread)
    # after all threads are finished result is merged
    for thread in threads:
        logging.debug('thread %s join', thread.name)
        thread.join()
    threads.clear()
    logging.info('extraction of data for %s ended.', extraction_data_name)


def extract_mapping_data(data_set, mapping_set, result_data, extraction_data_name):
    logging.info('extraction of mapping data for %s started.', extraction_data_name)
    thread_name = 0
    for mapping_set_item in mapping_set:
        thread_name = thread_name + 1
        thread = extractMappingDataThread(thread_name, mapping_set_item, data_set, result_data)
        logging.debug('thread %s started', thread.name)
        thread.start()
        threads.append(thread)
    # after all threads are finished result is merged
    for thread in threads:
        logging.debug('thread %s join', thread.name)
        thread.join()
    threads.clear()
    logging.info('extraction of mapping data for %s ended.', extraction_data_name)


def export_data(data, file_url):
    result = open(file_url, 'w')
    logging.info('start writing %s:', file_url)
    for item in data:
        result.write("%s" % str(item))
    logging.info('end writing %s:', file_url)


# result mappings
res_competitions = []
res_competition_mappings = []
res_competitors = []
res_competitor_mappings = []

# prod mappings
prod_competitions = []
prod_competitors = []

# pt1 mappings
pt1_competitions = []
pt1_competition_mappings = []
pt1_competitors = []
pt1_competitor_mappings = []

# sort out competitions
load_data(prod_competitions, 'prd/competitions.csv')
load_data(pt1_competitions, 'pt1/competitions.csv')
load_data(pt1_competition_mappings, 'pt1/competition_mappings.csv')
extract_data(pt1_competitions, prod_competitions, res_competitions, 'competitions')
export_data(res_competitions, 'competitions.csv')
extract_mapping_data(res_competitions, pt1_competition_mappings, res_competition_mappings, 'competition_mappings')
export_data(res_competition_mappings, 'competition_mappings.csv')

# sort out competitors
load_data(prod_competitors, 'prd/competitors.csv')
load_data(pt1_competitors, 'pt1/competitors.csv')
load_data(pt1_competitor_mappings, 'pt1/competitor_mappings.csv')
extract_data(pt1_competitors, prod_competitors, res_competitors, 'competitors')
export_data(res_competitors, 'competitors.csv')
extract_mapping_data(res_competitors, pt1_competitor_mappings, res_competitor_mappings, 'competitor_mappings')
export_data(res_competitor_mappings, 'competitor_mappings.csv')
